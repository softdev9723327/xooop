/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.xoooop;

/**
 *
 * @author asus
 */
public class Player {
    private char symbol;
    private int winCount , loseCount , drawCount;
    
    public Player(char symbol){
        this.symbol = symbol;
    }
    
    public char getSymbol(){
        return symbol;
    }
    
    public void win(){
        winCount++;
    }
    
    public void lose(){
        loseCount++;
    }
    
    public void draw(){
        drawCount++;
    }
    
    public int getwinCount(){
        return winCount;
    }
    
    public int getloseCount(){
        return loseCount;
    }
    
    public int getdrawCount(){
        return drawCount;
    }

    @Override
    public String toString() {
        return "Players{" + "symbol=" + symbol + ", winCount=" + winCount + ", loseCount=" + loseCount + ", drawCount=" + drawCount + '}';
    }
    
}
